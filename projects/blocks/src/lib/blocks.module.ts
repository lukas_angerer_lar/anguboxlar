import { NgModule } from '@angular/core';
import { BlocksComponent } from './blocks.component';



@NgModule({
  declarations: [BlocksComponent],
  imports: [
  ],
  exports: [BlocksComponent]
})
export class BlocksModule { }
