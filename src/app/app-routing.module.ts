import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RoutesToken } from './app-routes-core';
import { routes } from './app-routes';

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    {
      provide: RoutesToken,
      useValue: routes,
    }
  ]
})
export class AppRoutingModule { }
