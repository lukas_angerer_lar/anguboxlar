import { Component, Input } from '@angular/core';
import { ChainLinkProvider, ChainLink } from './injection-providers';

@Component({
  selector: 'app-child',
  template: `ChildComponent ({{link.fullName}} <- {{link.parentFullName}})
  <app-child *ngIf="level > 0" [level]="level - 1"></app-child>
  <app-child *ngIf="level > 1" [level]="level - 1"></app-child>
`,
  providers: [
    ChainLinkProvider.provide("Bar (child)"),
  ],
})
export class ChildComponent {
  @Input()
  public level: number;

  public constructor(public readonly link: ChainLink) {
    console.log(`${this.constructor.name} using chain link ${link.fullName}`);
  }
}
