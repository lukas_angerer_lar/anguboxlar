import { Component } from '@angular/core';
import { ChainLinkProvider, ChainLink } from './injection-providers';

@Component({
  selector: 'app-parent',
  template: `ParentComponent
  <div>
  <app-child [level]="1"></app-child>
  <app-child [level]="2"></app-child>
  <app-child [level]="1"></app-child>
</div>`,
  providers: [
    ChainLinkProvider.provide("Foo (parent)"),
  ],
})
export class ParentComponent {
  public constructor(link: ChainLink) {
    console.log(`${this.constructor.name} using chain link ${link.fullName}`);
  }
}
