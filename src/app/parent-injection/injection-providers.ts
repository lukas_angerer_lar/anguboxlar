import { Injector, SkipSelf, Injectable, Provider, InjectionToken, Inject, InjectFlags, Optional } from '@angular/core';

export const ChainLinkParentToken = new InjectionToken<ChainLink>('ChainLinkParentToken');

let linkId = 0;

export class ChainLink {
  public readonly id: number;

  constructor(public readonly name: string, private parent?: ChainLink) {
    this.id = linkId++;
    console.log(`created ChainLink ${this.fullName} value with parent ${this.parentFullName}`);
  }

  public get fullName(): string {
    return `"${this.name} (${this.id})"`;
  }

  public get parentFullName(): string {
    return this.parent ? this.parent.fullName : null;
  }
}

@Injectable()
export class ParentInjectorDummy {
  public constructor(public readonly injector: Injector) {
  }
}

export class ChainLinkProvider {
  public static provide(name: string): Provider {
    return [
      ParentInjectorDummy,
      {
        provide: ChainLink,
        deps: [[new Optional(), new SkipSelf(), ParentInjectorDummy]],
        useFactory: (dummy: ParentInjectorDummy): ChainLink => {
          const parent = dummy ? dummy.injector.get<ChainLink>(ChainLink) : null;

          return new ChainLink(name, parent);
        }
      },
    ];
  }
}
