import { Component } from '@angular/core';

@Component({
  selector: 'app-parent-injection-page',
  template: '<app-parent></app-parent>'
})
export class ParentInjectionPageComponent {
}
