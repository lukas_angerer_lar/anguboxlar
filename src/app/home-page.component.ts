import { Component, Inject } from '@angular/core';
import { RouteEx, RoutesToken } from './app-routes-core';

@Component({
  selector: 'app-home-page',
  template: `<ul>
  <li *ngFor="let route of visibleRoutes">
    <a [routerLink]="'/' + route.path" [routerLinkActive]="route.path">{{ route.metadata.title }}</a>
  </li>
</ul>`,
})
export class HomePageComponent {
  public readonly routes: RouteEx[];

  public constructor(@Inject(RoutesToken) routes: RouteEx[]) {
    this.routes = routes;
  }

  public get visibleRoutes(): RouteEx[] {
    return this.routes.filter(r => r.metadata.list);
  }
}
