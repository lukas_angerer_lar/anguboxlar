import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-lost-child',
  template: `<div>I am child {{ name }}<div>`,
})
export class LostChildComponent {
  @Input()
  public name: string;

  public constructor() {
  }
}
