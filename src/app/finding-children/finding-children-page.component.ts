import { Component, ContentChildren } from '@angular/core';

@Component({
  selector: 'app-finding-children-page',
  template: `
  <input type="checkbox" [(ngModel)]="condition" /> Condition
  <br /><br />
<app-children-host>
    <app-lost-child name="First Content"></app-lost-child>
    <ng-container>
        <app-lost-child name="Nested in ng-container"></app-lost-child>
    </ng-container>
    <ng-container *ngIf="condition">
        <app-lost-child name="Nested in conditional ng-container"></app-lost-child>
    </ng-container>
    <ng-container *ngIf="condition">
        <app-lost-child *ngIf="condition" name="Conditional nested in conditional ng-container"></app-lost-child>
    </ng-container>
    <app-lost-child name="Second Content"></app-lost-child>
</app-children-host>
`,
})
export class FindingChildrenPageComponent {
    public condition: boolean = true;
}
