import { Component, Input, ContentChildren, QueryList } from '@angular/core';
import { LostChildComponent } from './lost-child.component';

@Component({
  selector: 'app-children-host',
  template: `<app-lost-child name="First View"></app-lost-child>
<ng-content></ng-content>
<app-lost-child name="Second View"></app-lost-child>
<code>{{ found() | json }}</code>`,
})
export class ChildrenHostComponent {
    @ContentChildren(LostChildComponent, { descendants: false })
    private children: QueryList<LostChildComponent>;

    @ContentChildren(LostChildComponent, { descendants: true })
    private childrenDescendants: QueryList<LostChildComponent>;

    public constructor() {
    }

    public found(): object {
        return {
            children: this.children.map(c => ({ name: c.name })),
            childrenDescendants: this.childrenDescendants.map(c => ({ name: c.name }))
        };
    }
}
