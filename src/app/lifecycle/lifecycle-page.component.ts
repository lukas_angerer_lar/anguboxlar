import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { LifecycleLogger } from './lifecycle-logger';

@Component({
  selector: 'app-lifecycle-page',
  template: `
  <button (click)="onClick()">Do Stuff</button>
<app-lifecycle-container></app-lifecycle-container>
<div>
    <code><div *ngFor="let msg of messages">{{ msg }}</div></code>
</div>
`,
  changeDetection: ChangeDetectionStrategy.Default,
  providers: [
      LifecycleLogger,
  ]
})
export class LifecyclePageComponent {
    public constructor(private readonly logger: LifecycleLogger, private readonly cdr: ChangeDetectorRef) {
        this.logger.message.subscribe(m => {
            this.messages.push(m);
            this.cdr.markForCheck();
        });
    }

    public messages: string[] = [];

    public onClick(): void {
        console.log('clicked');
    }
}
