import {
    Component,
    OnInit,
    OnChanges,
    DoCheck,
    AfterContentInit,
    AfterViewInit,
    AfterViewChecked,
    AfterContentChecked,
    SimpleChanges,
    Inject,
} from '@angular/core';
import { LifecycleLogger } from './lifecycle-logger';
import { NameToken } from './name-token';

// tslint:disable-next-line: no-conflicting-lifecycle
@Component({
  selector: 'app-lifecycle-spy',
  template: ``,
})
export class LifecycleSpyComponent implements OnInit, OnChanges, AfterContentInit,
    AfterContentChecked, AfterViewInit, AfterViewChecked {

    protected log(...args) {
        if (this.name) {
            this.logger.log([this.constructor.name, this.name, ...args].join(' - '));
        } else {
            this.logger.log([this.constructor.name, ...args].join(' - '));
        }
    }

    public constructor(protected readonly logger: LifecycleLogger, @Inject(NameToken) public readonly name: string) {
        this.log('constructor');
    }

    public ngOnInit(): void {
        this.log('ngOnInit');
    }

    public ngOnChanges(changes: SimpleChanges): void {
        this.log('ngOnChanges', JSON.stringify(changes));
    }

    // public ngDoCheck(): void {
    //     this.log('ngDoCheck');
    // }

    public ngAfterContentInit(): void {
        this.log('ngAfterContentInit');
    }

    public ngAfterContentChecked(): void {
        this.log('ngAfterContentChecked');
    }

    public ngAfterViewInit(): void {
        this.log('ngAfterViewInit');
    }

    public ngAfterViewChecked(): void {
        this.log('ngAfterViewChecked');
    }
}
