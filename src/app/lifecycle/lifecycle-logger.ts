import { Observable, Subject } from 'rxjs';

export class LifecycleLogger {
    private messageSubject: Subject<string> = new Subject<string>();
    public message: Observable<string> = this.messageSubject.asObservable();

    public log(msg: string): void {
        console.log(msg);
        this.messageSubject.next(msg);
    }
}
