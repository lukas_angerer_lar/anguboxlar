import { Component, Input, Inject } from '@angular/core';
import { LifecycleSpyComponent } from './lifecycle-spy.component';
import { LifecycleLogger } from './lifecycle-logger';
import { NameToken } from './name-token';

@Component({
  selector: 'app-lifecycle-first-child',
  template: `<h2>First Child ({{ name }})</h2><div>DisplayValue: {{ displayValue }}</div>`,
})
export class LifecycleFirstChildComponent extends LifecycleSpyComponent {
    @Input()
    public displayValue: number;

    public constructor(logger: LifecycleLogger, @Inject(NameToken) name: string) {
        super(logger, name);
    }
}
