import { InjectionToken, Provider } from '@angular/core';

export const NameToken = new InjectionToken<string>('NameToken');

export function NameProvider(name: string): Provider {
    return {
        provide: NameToken,
        useValue: name,
    };
}
