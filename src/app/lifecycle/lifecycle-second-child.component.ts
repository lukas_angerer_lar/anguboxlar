import { Component, Input } from '@angular/core';
import { LifecycleSpyComponent } from './lifecycle-spy.component';
import { LifecycleLogger } from './lifecycle-logger';
import { NameProvider } from './name-token';

@Component({
  selector: 'app-lifecycle-second-child',
  template: `
<h2>Second Child</h2>
<div>Data: {{ data | json }}</div>
<app-lifecycle-first-child [displayValue]="data?.value"></app-lifecycle-first-child>
`,
  providers: [
    NameProvider('Nested')
  ]
})
export class LifecycleSecondChildComponent extends LifecycleSpyComponent {
    @Input()
    public data: { value: number };

    public constructor(logger: LifecycleLogger) {
        super(logger, null);
    }
}
