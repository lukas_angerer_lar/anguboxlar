import { Component } from '@angular/core';
import { LifecycleSpyComponent } from './lifecycle-spy.component';
import { LifecycleLogger } from './lifecycle-logger';
import { NameProvider } from './name-token';

@Component({
  selector: 'app-lifecycle-container',
  template: `
<h2>Container</h2>
<app-lifecycle-first-child [displayValue]="data.value"></app-lifecycle-first-child>
<app-lifecycle-second-child [data]="data"></app-lifecycle-second-child>`,
  providers: [
      NameProvider('TopLevel')
  ],
})
export class LifecycleContainerComponent extends LifecycleSpyComponent {
    public data = { value: 42, text: 'answer' };

    public constructor(logger: LifecycleLogger) {
        super(logger, null);
    }
}
