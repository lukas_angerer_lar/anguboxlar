import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page.component';

import { ParentInjectionPageComponent } from './parent-injection/parent-injection-page.component';
import { ParentComponent } from './parent-injection/parent.component';
import { ChildComponent } from './parent-injection/child.component';
import { LifecyclePageComponent } from './lifecycle/lifecycle-page.component';
import { LifecycleContainerComponent } from './lifecycle/lifecycle-container.component';
import { LifecycleFirstChildComponent } from './lifecycle/lifecycle-first-child.component';
import { LifecycleSecondChildComponent } from './lifecycle/lifecycle-second-child.component';
import { LibraryInheritancePageComponent } from './library-inheritance/library-inheritance-page.component';
import { CoreModule } from 'core';
import { BlocksModule } from 'blocks';
import { FindingChildrenPageComponent } from './finding-children/finding-children-page.component';
import { LostChildComponent } from './finding-children/lost-child.component';
import { ChildrenHostComponent } from './finding-children/children-host.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    ParentInjectionPageComponent,
    ParentComponent,
    ChildComponent,
    LifecyclePageComponent,
    LifecycleContainerComponent,
    LifecycleFirstChildComponent,
    LifecycleSecondChildComponent,
    LibraryInheritancePageComponent,
    FindingChildrenPageComponent,
    ChildrenHostComponent,
    LostChildComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    RouterModule,
    AppRoutingModule,
    CoreModule,
    BlocksModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
