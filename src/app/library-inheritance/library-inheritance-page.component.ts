import { Component } from '@angular/core';

@Component({
  selector: 'app-library-inheritance-page',
  template: `
<lib-core></lib-core>
<lib-blocks></lib-blocks>
`,
})
export class LibraryInheritancePageComponent {
}
