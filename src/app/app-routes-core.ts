import { InjectionToken } from '@angular/core';
import { Route } from '@angular/router';

export const RoutesToken = new InjectionToken<RouteEx[]>('RoutesToken');

export interface MetaRouteInfo {
  title: string;
  list?: boolean;
}

export interface RouteEx extends Route {
  metadata: MetaRouteInfo;
}
