import { RouteEx } from './app-routes-core';
import { HomePageComponent } from './home-page.component';
import { ParentInjectionPageComponent } from './parent-injection/parent-injection-page.component';
import { LifecyclePageComponent } from './lifecycle/lifecycle-page.component';
import { LibraryInheritancePageComponent } from './library-inheritance/library-inheritance-page.component';
import { FindingChildrenPageComponent } from './finding-children/finding-children-page.component';

export const routes: RouteEx[] = [
  {
    path: '',
    component: HomePageComponent,
    metadata: {
      title: 'Home',
      list: false,
    }
  },
  {
    path: 'parent-injection',
    component: ParentInjectionPageComponent,
    metadata: {
      title: 'Parent Injection',
      list: true,
    },
  },
  {
    path: 'lifecycle',
    component: LifecyclePageComponent,
    metadata: {
      title: 'Coponent Lifecycle',
      list: true,
    },
  },
  {
    path: 'library-inheritance',
    component: LibraryInheritancePageComponent,
    metadata: {
      title: 'Library Inheritance',
      list: true,
    },
  },
  {
    path: 'finding-children',
    component: FindingChildrenPageComponent,
    metadata: {
      title: 'Finding Children',
      list: true,
    },
  },
];
